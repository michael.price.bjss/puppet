class lamp {

# install apache2 package
package { 'apache2':
  ensure => installed,
}

# ensure apache2 service is running
service { 'apache2':
  require => Package['apache2'],			# Require the package to be installed before starting it
  ensure => running,
}

# install mysql-server package
package { 'mysql-server':
  ensure => installed,
}

# ensure mysql service is running
service { 'mysql':
  require => Package['mysql-server'],	# Require the package to be installed before starting it
  ensure => running,
}

# install php7.0 package
package { 'php7.0':
  require => Package['apache2'],
  ensure => installed,
}

# install php apache helper package
package { 'libapache2-mod-php':
  require => Package['php7.0'],
  ensure => installed,
}

# install php-mcrypt package
package { 'php-mcrypt':
  ensure => installed,
}

# install php-mysql package
package { 'php-mysql':
  ensure => installed,
}

# ensure info.php file exists
file { '/var/www/html/info.php':
  require => Package['apache2'],        # require 'apache2' package before creating
  ensure => file,
  content => '<?php  phpinfo(); ?>',    # display php default info page
}

# Ensure default index.html file is removed
file { '/var/www/html/index.html':
  ensure => absent,
}

}
