class cron-puppet {
    cron { 'puppet-apply':
        ensure  => present,
        command => "cd /etc/puppet; aws s3 sync s3://michaelprice-puppet/ .; /usr/bin/puppet apply /etc/puppet/manifests/site.pp",
        user    => root,
        minute  => '*/2'
    }
}
